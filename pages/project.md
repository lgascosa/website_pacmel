---
layout: page-fullwidth
show_meta: false
title: ""
subheadline: ""
teaser: ""
header: 
   title: Project
   background-color: "#334D5C"
permalink: "/project/"
---
# Project Objective
<div class="row">
  <div class="large-6 columns">      
  	Our goal is to develop a process-aware analytic framework for analyzing data from the sensors of machines and devices to enable the use of this data for process modeling and analysis, with the purpose of improving management and process transparency. In our research, we intend to apply a comprehensive approach by combining a top-down and a bottom-up directions.

	The top-down direction requires the definition of reference process models of machines and devices based on expert knowledge and the comparison of their phases to the states recorded in the data. In the bottom-up direction the phases will be determined based on data analysis and confronted with domain experts. This task aims at an in-depth understanding and analysis of the process by distinguishing the phases known to experts, identifying clearly occurring states that have not been defined but that exist in the process, although they are not reflected in the sensor data. Identification of the process phases and their introduction to adequate event logs will enable process modeling and analysis.  
  </div>
  <div class="large-6 columns">
     <div class=frame>
      <span class="helper"></span>
      <img src="https://thisispacmel.netlify.app/images/topdown_bottomup.JPG" style="vertical-align: middle; width:100%">
  </div>
  </div>
</div>


# Work Plan
We plan the implementation of the project in 6 Work Packages (WPs),

<div>
  <img src="https://thisispacmel.netlify.app/images/wp_table.jpg">
</div>


## Work Package 1 (WP1). Identification of requirements for smart factories.
### Lead: AGH, Duration: 9M (M1-9)

The initial objective of this WP is the analysis of case studies with respect to the Industry4.0/Smart Factory to identify the industrial requirements that will guide the development of a general process-aware analytics framework. To this end, an exploratory analysis of the related industrial datasets we possess will be conducted. This will include comparison to the considered industrial formats of the event logs in BPM that could be built based on this data, and investigation of process-aware schemes for data analytics. Moreover, we would provide a critical overview of the available open data sets in process mining and data mining from sensors. Finally, we will provide an analysis of the needs of various domains to create opportunities for further applications of our framework. 

## Work Package 2 (WP2). Knowledge extraction and data mining,.
### Lead: UAM, Duration: 12M (M4-15)

The main objectives of this WP include, the study and analysis of dimension reduction of the considered data sets, the selection of the appropriate granulation and abstraction level for the data analysis wrt sensor readings. We will provide the study of the adequate model representation of this low-level information extracted from sensors, possibly reconciling and integrating multiple heterogeneous data sources. The aim is to bring the data into a symbolic form that is suitable for interpretation via a domain ontology geared towards BPM, and make it amenable for the techniques in WP3. The data analysis and filtering will be provided to solve existing data quality problems, such as missing values and data redundancy, and to identify sources of unique states of factory machines/devices wrt sensor readings. Finally, the analysis and selection of appropriate machine learning algorithms, and data mining techniques, for data clusterization and classification to allow the extraction and discovery of new patterns will be performed. 

## Work Package 3 (WP3). Ontology driven interpretation.
### Lead: UNIBZ, Duration: 12M (M4-18)

The objective of this WP is to provide a conceptual and technological framework for the interpretation of the data produced as a result of WP2, in terms of the semantically meaningful elements that constitute the knowledge about the domain of interest. Such knowledge, suitably encoded in an ontology, provides the basis for addressing the following fundamental problems: How to identify relevant events with their attributes and resources? How to combine them in cases that are significant for the business process? And how to generate from the available data, device logs encoded in a standard format that is amenable for further processing by common process mining tools? To address these, we will devise suitable techniques based on characterizing events and logs in terms of queries over the domain ontology, and annotations and mappings between ontologies. We will build on and extend the ontology-based data access (OBDA) paradigm (with its techniques for efficient automatic query transformation and evaluation) to deal with the type of data at hand. 

## Work Package 4 (WP4). Process-aware analytics framework.
### Lead: UPM, Duration: 12M (M13-24)

The main outcomes of this WP include the provisioning of model mapping methods and visualization techniques that allow to relate the interpreted sensor data (provided by methods in WP3) to the process models; implementation of proof-of-concept software tools and the creation of feasibility studies for possible applications of the developed framework in mining as well as in other domains, including aeronautics, health-care, and development of requirements for integration of information from machine level sensor networks with BPM systems. The outcome of previous goal will be to demonstrate how the data regarding the current operation of machines can be efficiently incorporated into the existing BPM system. Moreover, the simulation and visualization in open BPM systems, such as [APROMORE](https://www.sciencedirect.com/science/article/pii/S0957417410013758), on selected use cases and data sets will be performed. This will include mapping various process abstraction into modeling notations according to industrial needs.

## Work Package 5 (WP5). Management.
### Lead: AGH, Duration: 24M (M1-24)

The activities will include organization and coordination of telecons (every 2 months) and project meetings (every 6 months). Moreover, an on-line work repository, based on GitLab installation used by AGH, will be set up and maintained. Furthermore, the participation in the yearly CHIST-ERA meetings will be provided, along with monitoring of the project progress and reporting. Finally, the risk monitoring, management and mitigation techniques will be implemented. 
## Work Package 6 (WP6). Dissemination and Exploitation.
### Lead: UNIBZ, Duration: 24M (M1-24)

The overall goal of the systematic efforts in this WP is to raise awareness and to foster participation in the related scientific communities and among industry stakeholders as well as to disseminate knowledge to research teams beyond the project consortium. Activities in this WP will include: the project website setup and maintenance; the (joint) scientific publications based on collaborative research work; the participation in scientific conferences, fairs and events, network and exchange with contiguous projects and initiatives; the publication of tools and methods developed in the project as well as of a subset of the studied dataset to allow reproducibility of project results. 