---
layout: page-fullwidth
show_meta: false
title: ""
subheadline: ""
teaser: ""
header: 
   title: Dissemination
   background-color: "#334D5C"
permalink: "/dissemination/"
---

Dissemination activities will be listed here as they take place:

### Publications

* V. Rodriguez-Fernandez, A. Trzcionkowska, A. Gonzalez-Pardo, E. Brzychczy, G. J. Nalepa and D. Camacho, **Conformance Checking for Time-Series-Aware Processes**, in *IEEE Transactions on Industrial Informatics*, vol. 17, no. 2, pp. 871-881, Feb. 2021. [Access to this paper](https://ieeexplore.ieee.org/document/9018187)



### Conferences



### Workshops


### Deliverables


### Code

