---
layout: page-fullwidth
show_meta: false
title: ""
subheadline: ""
teaser: ""
header: 
   title: Partners
   background-color: "#334D5C"
permalink: "/partners/"
---

### [AGH University of Science and Technology (AGH)](https://www.agh.edu.pl/en/)

<div class="row">
  <div class="large-9 columns">
      AGH is the project leader, and coordinates WP1 and WP5. Its expertise includes: industrial process management (including mining), and process modeling, data mining, machine learning from sensor data streams with uncertainty handling, activity recognition, symbolic knowledge representation. Related projects include: <a href="http://www.geist.re/pub:projects:bimloq:start">Bimloq</a> focused on BP modeling and optimization; <a href="http://www.prosecco.agh.edu.pl">Prosecco</a> that aimed at developing of business process modeling methods using semantic dependencies between diagram elements and data, and ontology-based mechanisms allowing for creating taxonomies of process concepts; <a href="https://www.geist.re/pub:projects:knowme:start">KnowMe</a> aimed at handling uncertainty in decision support and adaptation in mobile systems by integrating high level rule-based reasoning with data mining from streams of sensor data. We developed also an <a href="http://yadda.icm.edu.pl/baztech/element/bwmeta1.element.baztech-317c4bd5-a0b4-49ae-b2b0-61cd39b8a0ea">expert system for mining operation planning</a>, and a <a href="https://link.springer.com/chapter/10.1007/978-3-319-10894-0_23">calculation service for optimization of mining production</a>. 
  </div>
  <div class="large-3 columns">
     <div class=frame>
    	<span class="helper"></span>
     	<img src="https://thisispacmel.netlify.app/images/logo_AGH.png" title="logo agh" style="vertical-align: middle; width:60%">
	</div>
  </div>
</div>









### [Free University of Bozen-Bolzano (UNIBZ)](https://www.unibz.it/)

<div class="row">
  <div class="large-9 columns">
      Expertise of UNIBZ includes: Conceptual modeling of data and processes, Knowledge representation and reasoning, Ontology-based data access and integration, Process mining, Process monitoring. Selected related projects are: the Euregio Project on <a href="http://kaos.inf.unibz.it">Knowledge-Aware Operational Support (KAOS)</a>, the <a href="http://ontop.inf.unibz.it">Ontology-based data access framework (ONTOP)</a>, and the <a href="http://optique-project.eu">EU FP7 IP Project on Scalable End-User Access to Big Data (OPTIQUE)</a>. Members of the UNIBZ team are also leading <a href="https://noi.bz.it/en/infrastructure/labs/smart-data-factory">Smart Data Factory</a>, a lab that provides advice and technology transfer services in the areas of data processing and data science.    
  </div>
  <div class="large-3 columns">
    <div class=frame>
    	<span class="helper"></span>
	    <img src="https://thisispacmel.netlify.app/images/logo_UNIBZ.png" title="logo unibz" style="vertical-align: middle; width:60%">
	</div>
  </div>
</div>






### [Universidad Politécnica de Madrid (UPM)](https://www.upm.es/)

<div class="row">
  <div class="large-9 columns">
      Expertise of UAM includes: Big Data, Machine Learning: Clustering, and Hidden Markov Models, Classification and Deep Learning, Computational Intelligence: Evolutionary computation, Swarm Intelligence, Pattern and Process modeling and mining, Graph Computing, Social Mining, Behavioral Human-based modeling and mining, Simulation (traffic, IoT, processes), and Data Analysis for complex industrial applications for companies, i.e.: Airbus Defence & Space, Codice Technologies, ImpactWare, or Jobssy S.L. Selected related projects: tracking tool based on social media for risk assessment on radicalisation (<a href=" http://www.risk-track.eu/en/">RiskTrack</a>), CYBERSECURITY: data, information and risks (<a href="http://aida.ii.uam.es/projects/ciberdine">CIBERDINE</a>), semantic KEYword-based Search on sTructured data sOurcEs (<a href="http://www.cost.eu/COST_Actions/ict/IC1302">Keystone</a>), and Bioinspired Algorithms in Complex Ephemeral Environments (<a href="https://ephemech.wordpress.com">EphemeCH</a>).   
  </div>
  <div class="large-3 columns">
    <div class=frame>
    	<span class="helper"></span>
	  	<img src="https://thisispacmel.netlify.app/images/logo_UAM.png" title="logo upm" style="vertical-align: middle; width:60% ">
	</div>
  </div>
</div>

